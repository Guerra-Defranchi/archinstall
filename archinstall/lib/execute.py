#!/usr/bin/env python3

import shlex
import subprocess
import traceback

class Execute:

    def output_execute(self, command: str) -> str:
        try:
            execution = subprocess.run(shlex.split(command), stdout=subprocess.PIPE, text=True)
            return execution.stdout
        except subprocess.CalledProcessError as e:
            trace = traceback.format_exc()
            print(f"An error ocurred: {e}\nStack trace:\n{trace}")


    def bool_execute(self, command: str) -> bool:
        try:
            execution = subprocess.run(shlex.split(command), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
            if execution.returncode != 0:
                return False
            return True
        except subprocess.CalledProcessError as e:
            trace = traceback.format_exc()
            print(f"An error ocurred: {e}\nStack trace:\n{trace}")






