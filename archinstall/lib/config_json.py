#!/usr/bin/env python3

import json
import os
from pathlib import Path
from enum import Enum
from . import menu

class Files(Enum):
    SCHEMA = 'schema.json'
    CONFIG = 'config.json'


class ConfigJson:

    def __init__(self):
        self.path = Path(os.getcwd())

    def config_exists(self) -> bool:
        if (self.path / Files.CONFIG.value).exists():
            return True
        return False

    def create_config_file(self):
        if not self.config_exists(): 
            pass
            # COMMENTED TO AVOID CREATION WHILE TESTING
            # with open(self.path / Files.CONFIG.value, 'x') as f:
                # pass
        

