#/usr/bin/env python3

from typing import Dict

commands: Dict[str, str] = {
    "is_uefi": "cat /sys/firmware/efi/fw_platform_size",
    "net_connection": "ping -c 1 8.8.8.8",
    "disk_detection": "lsblk --json --nodeps --path --output NAME,SIZE,TYPE,RM",
    "efi_partition": "sgdisk --new  1::+500M --change-name=1:'EFI' DISK",
    "root_partition": "sgdisk --new 2::+50G  --change-name=2:'ROOT' DISK",
    "home_partition": "sgdisk --new 3::-0 --change-name=3:'HOME' DISK"
}


