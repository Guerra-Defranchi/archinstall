#!/usr/bin/env python3

from typing import Tuple, Dict, Any, Union 
from functools import cached_property
from . import device_info

class Menu:

    def __init__(self):
        self.disk = device_info.GetDeviceInfo()

    @cached_property
    def available_devices(self):
        return self.disk.clear_rm_devices()

    def disk_prompt(self) -> int:
        disks_available = self.available_devices
        num_disks = len(disks_available)
        print(f"{'Choose the disk for the installation:' if num_disks > 1 else 'Using the only diskavailable as default:'}")
        for index, value in enumerate(disks_available):
            print(f"[{index + 1}] Path: {value.get('name')} | Size: {value.get('size')}")
        return num_disks

    def disk_selection(self) -> int | None:
        if self.disk_prompt() <= 1:
            return None
        while True:
            try:
                user_input = input("\nEnter your option (number): ")
                user_option = int(user_input) - 1
                if 0 <= user_option < len(self.available_devices):
                    break
            except ValueError:
                print("Enter a valid option")
        return user_option

    def target_disk(self) -> Dict[str, str | bool]:
        option_disks = self.available_devices
        user_option = self.disk_selection()
        if not len(option_disks) > 1 and user_option is None:
            return option_disks[0]
        return option_disks[user_option]


