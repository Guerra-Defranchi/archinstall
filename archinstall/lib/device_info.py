#!/usr/bin/env python3

import subprocess
import json 
from typing import Dict, List, Any, Union
from . import execute
from . import commands


class GetDeviceInfo:

    def __init__(self):
        self.execute = execute.Execute()

    def get_lsblk_info(self) -> Dict[str, List[Any]]:
        lsblk_command = commands.commands.get('disk_detection')
        res = self.execute.output_execute(lsblk_command)
        return json.loads(res)

    def clear_rm_devices(self) -> List[Dict[str, str | bool]]:
        disks = self.get_lsblk_info().get("blockdevices")
        return [disk for disk in disks if disk.get("rm") == False]
        # RETURN STATEMENT BELOW COMMENTED FOR TESTING
        # return disks

