#!/usr/bin/env python3


import unittest
from lib import execute


class ExecuteClass(unittest.TestCase):


    def test_output_execute(self):
        execute_instance = execute.Execute()
        command = "echo 'Hello Archinstall!'"
        result = execute_instance.output_execute(command)
        self.assertEqual(result, 'Hello Archinstall!\n')


    def test_bool_execute(self):
        execute_instance = execute.Execute()
        command = "ls -l"
        result = execute_instance.bool_execute(command)
        self.assertEqual(result, True)

if __name__ == "__main__":
    unittest.main()
